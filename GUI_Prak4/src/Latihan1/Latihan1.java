package Latihan1;
import java.awt.Container;
import java.awt.FlowLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import static javax.swing.JFrame.EXIT_ON_CLOSE;
public class Latihan1 extends JFrame{
    private static final int FRAME_WIDTH = 300;
    private static final int FRAME_HEIGHT = 200;
    private static final int FRAME_X_ORIGIN = 150;
    private static final int FRAME_Y_ORIGIN = 250;
    private static final int BUTTON_WIDTH = 80;
    private static final int BUTTON_HEIGHT = 40;
    private JButton button;
    public static void main(String[] args){
        Latihan1 frame =  new Latihan1();
        frame.setVisible(true);
    }   
    public Latihan1(){
        Container contentPane = getContentPane();
        setSize ( FRAME_WIDTH, FRAME_HEIGHT );
        setResizable(false);
        setTitle("Program Ch7ButtonFrame");
        setLocation( FRAME_X_ORIGIN, FRAME_Y_ORIGIN );
        contentPane.setLayout(new FlowLayout());

        button = new JButton("Click Me");
        button.setSize(BUTTON_WIDTH, BUTTON_HEIGHT);
        contentPane.add(button);
        
        ButtonHandler handler = new ButtonHandler();
        button.addActionListener(handler);
        button.addActionListener(handler);
        
        setDefaultCloseOperation(EXIT_ON_CLOSE);
     }  
} 
