package Latihan4;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import static javax.swing.JFrame.EXIT_ON_CLOSE;
import javax.swing.JRootPane;
import javax.swing.JTextField;
public class Latihan4 extends JFrame implements ActionListener{
    private static final int FRAME_WIDTH = 300;
    private static final int FRAME_HEIGHT = 200;
    private static final int FRAME_X_ORIGIN = 150;
    private static final int FRAME_Y_ORIGIN = 250;
    private static final int BUTTON_WIDTH = 80;
    private static final int BUTTON_HEIGHT = 40;
    private JButton button;
    private JButton button2;
    private JTextField pesan;
    public static void main(String[] args){
        Latihan4 frame =  new Latihan4();
        frame.setVisible(true);
    }   
    public Latihan4(){
        Container contentPane = getContentPane();
        setSize ( FRAME_WIDTH, FRAME_HEIGHT );
        setResizable(false);
        setTitle("Program Ch7ButtonFrame");
        setLocation( FRAME_X_ORIGIN, FRAME_Y_ORIGIN );
        contentPane.setLayout(new FlowLayout());

        button = new JButton("Click Me");
        button.setSize(BUTTON_WIDTH, BUTTON_HEIGHT);
        contentPane.add(button);
        
        button2 = new JButton("Tombol 2");
        button2.setSize(BUTTON_WIDTH, BUTTON_HEIGHT);
        contentPane.add(button2);
        
        pesan = new JTextField();
        pesan.setColumns(20);
        contentPane.add(pesan);
        
        button.addActionListener(this);
        button2.addActionListener(this);
        
        setDefaultCloseOperation(EXIT_ON_CLOSE);
     }  
    @Override
    public void actionPerformed(ActionEvent event) {
        JButton buttonText = (JButton) event.getSource();
        
        JRootPane rootPane = buttonText.getRootPane();
        Frame frame = (JFrame) rootPane.getParent();
        
        if(buttonText.getText().equals("Click Me")){
           String isiPesan = pesan.getText();
           frame.setTitle(isiPesan);
        }else{
        frame.setTitle("You clicked " + buttonText.getText());
    }
} 
}
